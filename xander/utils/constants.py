START_TIME = 'start_time'
END_TIME = 'end_time'
DURATION = 'duration'

RUN_ID = 'run_id'
USER_ID = 'user_id'
USER_MAIL = 'user_mail'
HOSTNAME = 'hostname'
VERSION = 'version'
EXCEPTION = 'exception'

PIPELINE_SLUG = 'pipeline_slug'
PIPELINE_NAME = 'pipeline_name'

PROJECT_SLUG = 'project_slug'

PIPELINES = 'pipelines'