# Xander

Xander is my first experience in developing MLOps tools. I developed this tool one piece at time since my first day in Bip spa.
This tool represents my personal and professional growth, it contains scripts on which every Data Science project is based. Repeating them every time was frustrating so why not create a tool that did it for me?

Xander is a Greek name meaning "the one who saves".

At the moment, two types of pipelines are provided with Xander:
* General Purpose Pipelines developed to support all process that takes one or more inputs, process them and returns an output.
* Machine Learning Pipelines developed to support Machine Learning and Deep Learning models development and deployment.

### Initialize XanderEngine

### General Purpose Pipelines

### What's coming!