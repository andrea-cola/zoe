from sklearn.ensemble import RandomForestClassifier

from xander import XanderEngine


def ingestion(configuration, df):
    df = df.round(5)

    return df


def encoding(configuration, df):
    return df


def split(configuration, df):
    return df, df


def feature_selection(configuration, train, test):
    return train, test, []


def validation():
    return {}


def test_function():
    return {}


if __name__ == '__main__':
    configuration = {}

    zoe = XanderEngine()

    zoe.run()
