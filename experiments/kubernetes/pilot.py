from pymongo import MongoClient

MONGODB = 'mongodb://admin:n3wAetzKpg@35.192.211.33:27017/clempub?authMechanism=DEFAULT&authSource=admin'

if __name__ == '__main__':

    db = MongoClient(MONGODB).zoe

    items = db['projects']

    inserted_id = items.insert_one({
        "project_name": "PILOT",
        "project_owner": "andrea.cola@mail-bip.com",
        "project_description": "Progetto di predictive maintenance per Terna.",
        "project_repo": "",
        "project_slug": "terna-predictive-maintenance",
        "project_members": ["andrea.cola@mail-bip.com", "danilo.attuario@mail-bip.com", "davide.fantini@mail-bip.com", "federico.barusolo@mail-bip.com"]
    })
