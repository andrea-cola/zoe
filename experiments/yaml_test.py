import yaml
import docker


def generate_dockerfile():

    file_name = 'Dockerfile'
    f = open(file_name, 'w+')
    f.write('FROM ubuntu:latest')
    f.close()

    return True


if __name__ == '__main__':
    stream = open("workers.yaml", 'r')
    dictionary = yaml.safe_load(stream)

    print(dictionary)

    generate_dockerfile()

    # Initialize the docker environment
    client = docker.from_env()

    # Build client image
    client.images.build(path="./")