#!/usr/bin/env python
import json
import sys
from hashlib import md5
from getpass import getpass

import requests


def select_mode():

    while True:
        res = input("Do you want to enable offline mode? [Y/N] ")

        if 'Y' in res.upper():
            return 0

        if 'N' in res.upper():
            return 1


def authenticate():
    """
    Authenticate the user.
    """

    username = input("Your email: ")
    password = getpass()

    # Call the server and ask for an access token
    response = requests.post('http://127.0.0.1:5000/api/login', json={
        "username": username,
        "password": md5(password.encode('utf-8')).hexdigest()
    })

    # Check the status code
    if response.status_code != 200:
        print("Failed to init the project!")
        exit(0)

    # Get the access token form the response
    access_token = response.cookies['access_token']

    # Create a session and set the cookie
    s = requests.session()
    s.cookies.set("access_token", access_token)
    response = s.get('http://127.0.0.1:5000/api/get_projects')

    if response.status_code != 200:
        print("Failed to retrieve your projects!")
        exit(0)

    # Get the list of projects
    projects = response.json()

    print("\nProjects list:")
    # Print the projects
    for i, project in enumerate(projects):
        print(f'{i}) {project["project_name"]}')

    # Read the chosen project
    project_number = int(input("\nChoose the project you want to configure: "))

    if project_number >= len(projects):
        print("Please provide a correct number!")
        exit(0)

    # Use the number to select the project
    selected_project = projects[project_number]

    # Call the server to request a credentials dictionary
    response = s.post('http://127.0.0.1:5000/api/get_api_credentials', json={
        'project_slug': selected_project['project_slug']
    })

    if response.status_code != 200:
        print("Network error!")
        exit(0)

    # Read the response from the server
    zoe_credentials = response.json()

    # Save the credentials in a file
    json.dump(zoe_credentials, open('credentials.json', 'w'))


def configure_local_environment():
    """
    Configure the local environment generating the basic files and folders.
    """

    env = "XANDER_CREDENTIALS_FILE = 'credentials.json'\nXANDER_PROJECT_FILE = " \
          "'project.json'\nXANDER_CONFIGURATION_FILE = 'configuration.json'"

    f = open(".env", "w")
    f.write(env)
    f.close()

    # Configuration dictionary
    configuration_dict = {
        "local_repo": {
            "source": "",
            "destination": ""
        },
        "remote_repo": {
            "type": "",
            "source": "",
            "destination": ""
        },
        "server": {
            "local_mode": False,
            "ip_address": "35.192.211.33"
        }
    }

    # Create the file with the configuration
    json.dump(configuration_dict, open('configuration.json', 'w'))

    # Create the file for the project
    json.dump({}, open('project.json', 'w'))


if sys.argv[1] == 'init':

    # Mode selection
    mode = select_mode()

    # Authenticate the user
    if mode == 1:
        authenticate()

    # Create basic folders and files
    configure_local_environment()

    print("\nConfiguration conpleted!")
